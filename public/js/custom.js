$(document).ready(function() {
    var count = 0;
    var area = $('#propertyDIV');
    $('#addProperty').click(function (e) {
        e.preventDefault();
        count ++;
        $(area).append('<div id="form'+count+'"></div>');
        var form = $('#form'+count);

        $(form).append('<div class="form-group"><input class="form-control" type="text" placeholder="Address" name="form['+count+']"/></div>');
        $(form).append('<div class="form-group"><input class="form-control" type="text" placeholder="Postcode" name="form['+count+']"/></div>');
        $(form).append('<div class="form-group"><input class="form-control" type="text" placeholder="Bed Rooms" name="form['+count+']"/></div>');
        $(form).append('<div class="form-group"><input class="form-control" type="text" placeholder="Bed Rooms" name="form['+count+']"/></div>');
        $(form).append('<div class="form-group"><input class="form-control" type="text" placeholder="£ Price" name="form['+count+']"/></div>');
        $(form).append('<a href="#" class="remove"><i class="fa fa-minus" aria-hidden="true"></i></a>');
    });

    $(area).on("click",".remove", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); count--;
    });


    var x =0;
    var imageArea = $('#image');
    $('#addImage').click(function (e) {
        e.preventDefault();
        x++;
        $(imageArea).append('<div id="area'+x+'"></div>');
        var tem = $('#area'+x);
        $(tem).append('<input class="form-control" type="file"  name="image['+x+']"/>');
        $(tem).append('<a href="#" class="remove"><i class="fa fa-minus" aria-hidden="true"></i></a>');

    });

    $(imageArea).on("click",".remove", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });

    $('#forrent').click(function(e) {
        $("#rentFrom").delay(100).fadeIn(100);
        $("#saleForm").fadeOut(100);
        $('#forsale').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#forsale').click(function(e) {
        $("#saleForm").delay(100).fadeIn(100);
        $("#rentFrom").fadeOut(100);
        $('#forrent').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

    $( "#success" ).fadeOut( 2000 );

});