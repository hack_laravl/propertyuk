<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->unique();
            $table->string('address');
            $table->string('postcode');
            $table->string('property_type');
            $table->integer('bed_rooms');
            $table->string('category');
            $table->string('notes');
            $table->decimal('price', 11,2);
            $table->tinyInteger('status');
            $table->tinyInteger('publish');
            $table->unsignedInteger('landlord_id');
            $table->timestamps();

            $table->foreign('landlord_id')->references('id')->on('landlords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
