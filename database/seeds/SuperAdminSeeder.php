<?php

use Illuminate\Database\Seeder;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'Razmy789',
            'email' => 'razmyrazzak@gmail.com',
            'password' => Hash::make('bittrh2D')
        ]);
    }
}
