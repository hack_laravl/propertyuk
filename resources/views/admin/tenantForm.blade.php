@extends('admin.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-8 col-lg-offset-2">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <h2>Create Tenant</h2>
                {!! Form::open( array( 'url'=>'createTenant' , 'files' => true ) ) !!}
                <div class="form-group">
                    <label>Name</label>
                    {!! Form::text( 'name', null, array( 'class' => 'form-control','placeholder' => 'Name' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Email</label>
                    {!! Form::text( 'email', null, array( 'class' => 'form-control','placeholder' => 'Email' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Address</label>
                    {!! Form::text( 'address',null, array( 'class' => 'form-control','placeholder' => 'Address' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Select Property</label>
                    {!! Form::select('property', array(null=>'Please select'), null, array('class' => 'form-control') ) !!}
                </div>
                <div class="form-group">
                    <label>Post Code</label>
                    {!! Form::text( 'postcode',null, array( 'class' => 'form-control','placeholder' => 'Post Code' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    {!! Form::text( 'phone',null, array( 'class' => 'form-control','placeholder' => 'Phone' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Notes</label>
                    {!! Form::textarea ( 'notes', null, array( 'class' => 'form-control' ,'placeholder' => 'Additional notes' ) ) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit( 'submit', array( 'class'=>'btn btn-primary', 'id'=>'login-submit', 'value'=>'Create' ) ) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection