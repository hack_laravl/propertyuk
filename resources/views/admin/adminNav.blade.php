@if(Auth::check())
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Property<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::to('showPropertyForm') }}">Create</a></li>
                        <li><a href="{{ URL::to('admin') }}">List of Properties</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Landlord<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::to('showPropertyForm') }}">Create</a></li>
                        <li><a href="{{ URL::to('listoflandlords') }}">List of Landlords</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tenant<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::to('showTenantFrom') }}">Create</a></li>
                        <li><a href="#">List of Tenants</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ URL::to('showCreateForm') }}"  role="button" aria-haspopup="true" aria-expanded="false">Create Account</a>

                </li>
                <li class="dropdown">
                    <a href="{{ URL::to('showIntrests') }}"  role="button" aria-haspopup="true" aria-expanded="false">Show Users Interests</a>

                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Username: {{Auth::user()->username}}</a></li>
                <li><a href="{{ URL::to('logout') }}"  role="button" aria-haspopup="true" aria-expanded="false">Logout</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>
@endif