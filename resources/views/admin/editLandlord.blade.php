@extends('admin.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-8 col-lg-offset-2">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('status'))
                    <div id="success" class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <h2>Edit Landlord</h2>
                    {!! Form::open( array( 'url'=>'updateLandlord' ) ) !!}
                    {!! Form::hidden( 'id',$landlord->id ) !!}
                    <div class="form-group">
                        <label>Name</label>
                        {!! Form::text( 'name', $landlord->name, array( 'class' => 'form-control','placeholder' => 'Name' ) ) !!}
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        {!! Form::text( 'email', $landlord->email, array( 'class' => 'form-control','placeholder' => 'Email' ) ) !!}
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        {!! Form::text( 'landlord_address',$landlord->landlord_address, array( 'class' => 'form-control','placeholder' => 'Address' ) ) !!}
                    </div>
                    <div class="form-group">
                        <label>Post Code</label>
                        {!! Form::text( 'landlord_postcode',$landlord->landlord_postcode, array( 'class' => 'form-control','placeholder' => 'Post Code' ) ) !!}
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        {!! Form::text( 'phone_number',$landlord->phone_number, array( 'class' => 'form-control','placeholder' => 'Phone' ) ) !!}
                    </div>
                    <div class="form-group">
                        <label>Notes</label>
                        {!! Form::textarea ( 'landlord_notes', $landlord->landlord_notes, array( 'class' => 'form-control' ,'placeholder' => 'Additional notes' ) ) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit( 'Update', array( 'class'=>'btn btn-primary', ) ) !!}
                        <a class="btn btn-danger" href="#" role="button">Delete</a>
                    </div>
                    {!! Form::close() !!}

                    <h2>Properties</h2>
                    <a class="btn btn-primary" target="_blank" href="{{ URL::to('showaddPropertyform') }}/{{$landlord->id}}" role="button">+ Add Property</a>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Reference</th>
                            <th>Address</th>

                        </tr>
                        </thead>
                        @foreach($landlord->properties as $property)
                            <tr>
                                <td>{{$property['reference']}}</td>
                                <td>{{$property['address']}}</td>

                                <td>
                                <a class="btn btn-primary" href="{{ URL::to('editPropertyFrom') }}/{{$property['id']}}" role="button">Edit</a>
                                <a class="btn btn-danger" href="{{ URL::to('deleteProperty') }}/{{$property['id']}}" role="button">Delete</a>
                                @if($property['publish'] == 1)
                                <a class="btn btn-warning" href="{{ URL::to('togglePublishStatus') }}/{{$property['id']}}" role="button">Unpublished</a>
                                @else
                                <a class="btn btn-warning" href="{{ URL::to('togglePublishStatus') }}/{{$property['id']}}" role="button">Publish</a>
                                @endif


                                </td>

                            </tr>
                        @endforeach
                    </table>
            </div>

        </div>
    </div>


@endsection