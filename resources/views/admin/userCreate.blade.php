@extends('admin.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-8 col-lg-offset-2">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('status'))
                    <div id="success" class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <h2>Create Admin Account</h2>
                {!! Form::open( array( 'url'=>'createUser' ) ) !!}

                <div class="form-group">
                    <label>Username</label>
                    {!! Form::text( 'username', null, array( 'class' => 'form-control','placeholder' => 'Username' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Email</label>
                    {!! Form::text( 'email', null, array( 'class' => 'form-control','placeholder' => 'Email' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Password</label>
                    {!! Form::password( 'password', array( 'class' => 'form-control','placeholder' => 'Password' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    {!! Form::password( 'password_confirmation', array( 'class' => 'form-control','placeholder' => 'Confirm Password' ) ) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit( 'submit', array( 'class'=>'btn btn-primary', 'id'=>'login-submit', 'value'=>'Create' ) ) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection