@extends('admin.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-lg-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('status'))
                    <div id="success" class="alert alert-success">
                        {{ session('status') }}
                    </div>
                 @endif
                <h2>List of Landlords</h2>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Address</th>
                        <th>Post Code</th>
                        <th>Name</th>
                        <th>Phone Number</th>
                        <th>Number of Properties</th>
                        <th>Created on</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    @foreach($lisoflandlords as $lisoflandlord)
                        <tr @if($lisoflandlord->status == 1)class="success"@endif>
                            <td>{{$lisoflandlord->reference}}</td>
                            <td>{{$lisoflandlord->landlord_address}}</td>
                            <td>{{$lisoflandlord->landlord_postcode}}</td>
                            <td>{{$lisoflandlord->name}}</td>
                            <td>{{$lisoflandlord->phone_number}}</td>
                            <td>{{count($lisoflandlord->properties)}}</td>
                            <td>{{$lisoflandlord->created_at}}</td>
                            <td>
                                <a class="btn btn-primary" href="{{ URL::to('editLandlord') }}/{{$lisoflandlord->id}}" role="button">Edit/View</a>
                                <a class="btn btn-danger" href="{{ URL::to('deleteLandlord') }}/{{$lisoflandlord->id}}" role="button">Delete</a>
                                {{--@if($lisoflandlord->status == 1)--}}
                                    {{--<a class="btn btn-warning" href="{{ URL::to('togglePublishStatus') }}/{{$lisoflandlord->id}}" role="button">Deactivate</a>--}}
                                {{--@else--}}
                                    {{--<a class="btn btn-warning" href="{{ URL::to('togglePublishStatus') }}/{{$lisoflandlord->id}}" role="button">Activate</a>--}}
                                {{--@endif--}}


                            </td>

                        {{--</tr>--}}
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection