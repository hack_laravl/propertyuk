@extends('admin.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-lg-12">
                <h2>List of Property Interests</h2>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>email</th>
                        <th>Phone</th>
                        <th>Property Refference</th>
                        <th>Property Address</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    @foreach($propertIntersts as $propertInterst)
                        <tr>
                            <td>{{$propertInterst->id}}</td>
                            <td>{{$propertInterst->email}}</td>
                            <td>{{$propertInterst->phone}}</td>
                            <td>{{$propertInterst->property_reference}}</td>
                            <td>{{$propertInterst->property_address}}</td>
                            <td></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection