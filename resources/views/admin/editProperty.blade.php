@extends('admin.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-8 col-lg-offset-2">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <div class="col-lg-4">
                        <h2>Landlord Infomation</h2>
                        <div class="form-group">
                            <label>Name:</label>
                            {{ $property->landlord['name'] }}
                        </div>
                        <div class="form-group">
                            <label>Reff:</label>
                            {{ $property->landlord['reference'] }}
                        </div>
                        <div class="form-group">
                            <label>Email:</label>
                            {{ $property->landlord['email'] }}
                        </div>
                        <div class="form-group">
                            <label>Phone:</label>
                            {{ $property->landlord['phone_number'] }}
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <h2>Edit Property</h2>
                        {!! Form::open( array( 'url'=>'updateProperty' , 'files' => true ) ) !!}
                        {!! Form::hidden( 'id',$property->id ) !!}
                        <div class="form-group">
                            <label>Address</label>
                            {!! Form::text( 'address',$property->address, array( 'class' => 'form-control','placeholder' => 'Address' ) ) !!}
                        </div>
                        <div class="form-group">
                            <label>Post code</label>
                            {!! Form::text( 'postcode',$property->postcode, array( 'class' => 'form-control','placeholder' => 'Post Code' ) ) !!}
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            {!! Form::number( 'price',$property->price, array( 'class' => 'form-control','placeholder' => 'Price' ) ) !!}
                        </div>
                        <div class="form-group">
                            <label>Price tag</label>
                            {!! Form::text( 'price_tag',$property->price_tag, array( 'class' => 'form-control','placeholder' => 'Price Tag' ) ) !!}
                        </div>

                        <div class="form-group">
                            <label>Number of Beds</label>
                            {!! Form::text( 'bed_rooms',$property->bed_rooms, array( 'class' => 'form-control','placeholder' => 'Number of bed rooms' ) ) !!}
                        </div>
                        <div class="form-group">
                            <label>Number of Bath Rooms</label>
                            {!! Form::text( 'bath_rooms',$property->bath_rooms, array( 'class' => 'form-control','placeholder' => 'Number of Bath Rooms' ) ) !!}
                        </div>
                        <div class="form-group">
                            <label>Number of Receptions</label>
                            {!! Form::text( 'reception',$property->reception, array( 'class' => 'form-control','placeholder' => 'Number of Receptions' ) ) !!}
                        </div>
                        <div class="form-group">
                            <label>Property Type</label>
                            {!! Form::select('property_type', array(null=>'Please select','Houses'=>'Houses','Flats'=>'Flats'), $property->property_type , array('class' => 'form-control') ) !!}
                        </div>
                        @if($property->publish == 0)
                            <div class="form-group">
                                <label>Publish Property</label>
                                {!! Form::checkbox( 'publish', 1) !!}
                            </div>
                        @elseif($property->publish == 1)
                            <div class="form-group">
                                <label>Publish Property</label>
                                {!! Form::checkbox( 'publish', 0, true ) !!}
                            </div>
                        @endif
                        <div class="form-group">
                            <label>Category</label>
                            {!! Form::select('category', array(null=>'Please select','For Sale'=>'For Sale','To Rent'=>'To Rent'), $property->category , array('class' => 'form-control') ) !!}
                        </div>
                        <div class="form-group">
                            <label>Notes</label>
                            {!! Form::textarea ( 'notes', $property->notes, array( 'class' => 'form-control' ,'placeholder' => 'Additional notes' ) ) !!}
                        </div>

                        <div id="image" class="form-group">
                            <label>Image</label>
                            <a id="addImage" href="#"><i class="fa fa-plus" aria-hidden="true"></i> add image</a>
                            {!! Form::file('image[0]',array( 'class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit( 'Update', array( 'class'=>'btn btn-primary', 'id'=>'login-submit', 'value'=>'Create' ) ) !!}
                        </div>
                        {!! Form::close() !!}
                        @if(count($property->images)>0)
                            <h2>Images</h2>
                            {!! Form::open( array( 'url'=>'deleteImage', 'method'=> 'GET') ) !!}
                            {!! Form::hidden( 'id',$property->id ) !!}
                            <ul class="list-unstyled imageColumn">
                                @foreach($property->images as $image)
                                    <li>
                                        <img src="{{ asset("uploads") }}/{{$image->file}}" class="img-thumbnail" alt="...">
                                        <input type="checkbox" name="image[{{$image->id}}]" value="{{$image->id}}" class="form-check-input">
                                    </li>
                                @endforeach
                            </ul>
                            <div class="form-group">
                                {!! Form::submit( 'Delete', array( 'class'=>'btn btn-danger', ) ) !!}
                            </div>
                            {!! Form::close() !!}
                        @endif
                    </div>

            </div>
        </div>
    </div>

@endsection