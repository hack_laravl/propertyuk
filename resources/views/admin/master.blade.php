<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>propertypointuk-admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="propertypointuk">
    <meta name="razmy" content="propertypointuk">


    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("css/bootstrap-theme.css") }}" rel="stylesheet">
    <link href="{{ asset("css/adminStyle.css") }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- The fav icon -->
    <link rel="shortcut icon" href="">
</head>
<body>
@include('admin.adminNav')
@yield('content')
<script src="{{ asset("js/jquery-3.1.1.min.js") }}"></script>
<script src="{{ asset("js/bootstrap.min.js") }}"></script>
<script src="{{ asset("js/custom.js") }}"></script>
<script src="{{ asset("BootstrapForm/dist/js/bootstrap-formhelpers.js") }}"></script>
<script src="{{ asset("BootstrapForm/js/bootstrap-formhelpers-states.js") }}"></script>
<script src="{{ asset("BootstrapForm/js/bootstrap-formhelpers-countries.js") }}"></script>
</body>
</html>
