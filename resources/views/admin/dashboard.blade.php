@extends('admin.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-lg-12">
                <h2>List of Properties</h2>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Address</th>
                        <th>Post Code</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Property Type</th>
                        <th>Landlord ID</th>
                        <th>Landlord Name</th>
                        <th>Landlord Phone</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    @foreach($listOfProperties as $listOfProperty)
                        <tr @if($listOfProperty->publish == 1)class="success"@endif>
                            <td>{{$listOfProperty->reference}}</td>
                            <td>{{$listOfProperty->address}}</td>
                            <td>{{$listOfProperty->postcode}}</td>
                            <td>{{$listOfProperty->price}}</td>
                            <td>{{$listOfProperty->category}}</td>
                            <td>{{$listOfProperty->property_type}}</td>
                            <td>{{$listOfProperty->landlord['reference']}}</td>
                            <td>{{$listOfProperty->landlord['name']}}</td>
                            <td>{{$listOfProperty->landlord['phone_number']}}</td>
                            <td>
                                <a class="btn btn-primary" href="{{ URL::to('editPropertyFrom') }}/{{$listOfProperty->id}}" role="button">Edit</a>
                                <a class="btn btn-danger" href="#" role="button">Delete</a>
                                @if($listOfProperty->publish == 1)
                                    <a class="btn btn-warning" href="{{ URL::to('togglePublishStatus') }}/{{$listOfProperty->id}}" role="button">Unpublished</a>
                                @else
                                    <a class="btn btn-warning" href="{{ URL::to('togglePublishStatus') }}/{{$listOfProperty->id}}" role="button">Publish</a>
                                @endif


                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection