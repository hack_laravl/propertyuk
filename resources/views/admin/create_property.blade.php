@extends('admin.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-8 col-lg-offset-2">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('status'))
                    <div id="success" class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                    <h2>Create Landlord</h2>
                {!! Form::open( array( 'url'=>'createProperty' , 'files' => true ) ) !!}
                <div class="form-group">
                    <label>Name</label>
                    {!! Form::text( 'name', null, array( 'class' => 'form-control','placeholder' => 'Name' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Email</label>
                    {!! Form::text( 'email', null, array( 'class' => 'form-control','placeholder' => 'Email' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Address</label>
                    {!! Form::text( 'landlord_address',null, array( 'class' => 'form-control','placeholder' => 'Address' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Post Code</label>
                    {!! Form::text( 'landlord_postcode',null, array( 'class' => 'form-control','placeholder' => 'Post Code' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    {!! Form::text( 'phone_number',null, array( 'class' => 'form-control','placeholder' => 'Phone' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Notes</label>
                    {!! Form::textarea ( 'landlord_notes', null, array( 'class' => 'form-control' ,'placeholder' => 'Additional notes' ) ) !!}
                </div>

                    <h2>Create Property</h2>

                <div class="form-group">
                    <label>Address</label>
                    {!! Form::text( 'address',null, array( 'class' => 'form-control','placeholder' => 'Address' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Post code</label>
                    {!! Form::text( 'postcode',null, array( 'class' => 'form-control','placeholder' => 'Post Code' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Town</label>
                    {!! Form::text( 'town',null, array( 'class' => 'form-control','placeholder' => 'Town' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Price</label>
                    {!! Form::text( 'price',null, array( 'class' => 'form-control','placeholder' => 'Price' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Price tag</label>
                    {!! Form::text( 'price_tag',null, array( 'class' => 'form-control','placeholder' => 'Price Tag eg: PCM' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Number of Bed Rooms</label>
                    {!! Form::text( 'bed_rooms',null, array( 'class' => 'form-control bfh-number','placeholder' => 'Number of bed rooms' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Number of Bath Rooms</label>
                    {!! Form::text( 'bath_rooms',null, array( 'class' => 'form-control bfh-number','placeholder' => 'Number of Bath Rooms' ) ) !!}

                </div>
                <div class="form-group">
                    <label>Number of Receptions</label>
                    {!! Form::text( 'reception',null, array( 'class' => 'form-control bfh-number','placeholder' => 'Number of Receptions' ) ) !!}
                </div>
                <div class="form-group">
                    <label>Property Type</label>
                    {!! Form::select('property_type', array(null=>'Please select','Houses'=>'Houses','Flats'=>'Flats'), null , array('class' => 'form-control') ) !!}
                </div>
                <div class="form-group">
                    <label>Category</label>
                    {!! Form::select('category', array(null=>'Please select','For Sale'=>'For Sale','To Rent'=>'To Rent'), null , array('class' => 'form-control') ) !!}
                </div>
                <div class="form-group">
                    <label>Notes</label>
                    {!! Form::textarea ( 'notes', null, array( 'class' => 'form-control' ,'placeholder' => 'Additional notes' ) ) !!}
                </div>
                <div id="image" class="form-group">
                    <label>Image</label>
                    <a id="addImage" href="#"><i class="fa fa-plus" aria-hidden="true"></i> add image</a>
                    {!! Form::file('image[0]',array( 'class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit( 'submit', array( 'class'=>'btn btn-primary', 'id'=>'login-submit', 'value'=>'Create' ) ) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection