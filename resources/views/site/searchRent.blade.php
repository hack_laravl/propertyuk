@extends('site.master')
@section('content')
    <div class="container-fluid property_box">
        <div class="row box">
            @foreach( $properties as $property )
                @inject('thumnail', 'App\Http\Controllers\SiteController')
                <div class="bg-image">
                    <a href="{{ URL::to('propertyById') }}/{{ $property->id }}">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <div class="bg_white">
                            <img src="{{ asset("uploads") }}/{{$thumnail->getImageForSite( $property->id )}}" class="img-responsive">
                            <div class="property_price">
                                <span>£{{$property->price}} PCM</span>
                            </div>
                            <div class="property_detail">
                                <ul class="list-unstyled">
                                    <li>{{$property->reference}}</li>
                                    <li>{{$property->category}}</li>
                                    <li>{{$property->property_type}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection