<div class="form-group">
    <label>Search</label>
    {!! Form::text( 'search', null, array( 'class' => 'form-control' ,'placeholder' => 'eg: SM4 4LE, 10 Down road....' ) ) !!}
</div>
<div class="row">
    <div class="col-lg-6 col-md-6">
        <ul class="list-unstyled">
            <li>
                <div class="">
                    <div class="form-group">
                        <label>Property type:</label>
                        {!! Form::select('property_type', array(null=>'Please select','Houses'=>'Houses','Flats'=>'Flats'), null , array('class' => 'form-control') ) !!}
                    </div>
                </div>
            </li>
            <li>
                <div class="">
                    <div class="form-group">
                        <label>Bed Rooms:</label>
                        {!! Form::text('bed_rooms', null, array( 'class' => 'form-control bfh-number','placeholder' => 'Bed of Receptions') ) !!}
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="col-lg-6 col-md-6">
        <ul class="list-unstyled">
            <li>
                <div class="">
                    <div class="form-group">
                        <label>Bath Rooms:</label>
                        {!! Form::text('bath_rooms',null, array( 'class' => 'form-control bfh-number','placeholder' => 'Bath of Receptions') ) !!}
                    </div>
                </div>
            </li>
            <li>
                <div class="">
                    <div class="form-group">
                        <label>Receptions:</label>
                        {!! Form::text( 'reception',null, array( 'class' => 'form-control bfh-number','placeholder' => 'Number of Receptions' ) ) !!}
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>