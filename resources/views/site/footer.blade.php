<!-- Footer -->
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="footer_style">
                {{--<div class="hidden-xs col-lg-12">--}}
                    {{--<h1>Explore PropetyPoint</h1>--}}
                    {{--<hr>--}}
                    {{--<ul class="list-unstyled footer_list">--}}
                        {{--<li><a href="#">Help</a></li>--}}
                        {{--<li><a href="#">Contact us</a></li>--}}
                        {{--<li><a href="#">For Sale</a></li>--}}
                        {{--<li><a href="#">For Rent</a></li>--}}
                        {{--<li><a href="#">Sitemap</a></li>--}}
                        {{--<li><a href="#">Privacy</a></li>--}}
                        {{--<li><a href="#">Hous Home</a></li>--}}
                        {{--<li><a href="#">Commmercial</a></li>--}}
                        {{--<li><a href="#">Overseas</a></li>--}}
                        {{--<li><a href="#">Property News</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}

                <div class="hidden-lg hidden-md hidden-sm col-lg-12">
                    <h1>Explore PropetyPoint</h1>
                    <hr>
                    <ul class="list-unstyled footer_list">
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">For Sale</a></li>
                </div>

                <div class="col-lg-12">
                    <h1>Contact us</h1>
                    <hr>
                </div>
                <div class="col-md-4 col-sm-6">
                    <p><i class="fa fa-map-pin"></i> 81 London Road, North Cheam, Surrey SM3 9AE</p>
                    <p><i class="fa fa-phone"></i> Phone 0871 976 5405</p>
                    <p><i class="fa fa-envelope"></i> E-mail : info@propertypointuk.com</p>
                    <p><i class="fa fa-envelope"></i> Sales E-mail : sales@propertypointuk.comm</p>
                </div>
            </div>

        </div>
    </div>
</footer>

<div class="copyright">
    <div class="container-fluid">
        <div class="col-md-6">
            <p>© 2016 - All Rights with Property Point UK</p>
        </div>
    </div>
</div>

