<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top custom_nav" role="navigation">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="hidden-xs " href="{{ URL::to('/') }}"><img class="" src="{{ asset("img/logo.png") }}" alt=""></a>
                <a class="hidden-lg hidden-md hidden-sm center_log"  href="#"><img class=""  src="{{asset("img/logo50.png")}}" alt=""></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li  ><a class="{{ Request::is('/') ? 'activeCutom' : '' }}" href="{{ URL::to('/') }}">Home</a></li>
                    <li><a class="{{ Request::is('search') ? 'activeCutom' : '' }}" href="{{ URL::to('search') }}">Properties</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- Brand and toggle get grouped for better mobile display -->

    </div>
    <!-- /.container -->
</nav>
