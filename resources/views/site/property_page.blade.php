@extends('site.master')
@section('content')
    <div class="container">
        <div class="row propertyshow_box">
            @inject('images', 'App\Http\Controllers\SiteController')
            {{--{{var_dump($property)}}--}}
            {{--{{var_dump($images->getAllImageForProperty($property->id))}}--}}
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bg_white">
                <div class="col-xs-12 col-lg-12">
                    <ul class="list-unstyled">
                        <li><h2>{{$property->address}},{{$property->postcode}} ({{$property->category}})</h2></li>
                        <li class="price_nomargin"><p> {{$property->price}} {{$property->price_tag}}</p></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
                    <div id="jssor_1" style="position:relative;top:0px;left:0px;width:900px;height:400px;overflow:hidden;visibility:hidden;background-color:#24262e;">
                        <!-- Loading Screen -->
                        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                        </div>
                        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:240px;width:720px;height:480px;overflow:hidden;">
                            @foreach($images->getAllImageForProperty($property->id) as $image)
                                <div>
                                    <img data-u="image" src="{{ asset("uploads") }}/{{$image['file']}}" />
                                    <img data-u="thumb" src="{{ asset("uploads") }}/{{$image['file']}}" />
                                </div>

                            @endforeach
                                @if($images->getAllImageForProperty($property->id)->isEmpty())
                                    <div>
                                        <img data-u="image" src="{{ asset("uploads/default.png") }}" />
                                        <img data-u="thumb" src="{{ asset("uploads/default.png") }}" />
                                    </div>
                                @endif

                        </div>
                        <!-- Thumbnail Navigator -->
                        <div data-u="thumbnavigator" class="jssort01-99-66" style="position:absolute;left:0px;top:0px;width:240px;height:480px;" data-autocenter="2">
                            <!-- Thumbnail Item Skin Begin -->
                            <div data-u="slides" style="cursor: default;">
                                <div data-u="prototype" class="p">
                                    <div class="w">
                                        <div data-u="thumbnailtemplate" class="t"></div>
                                    </div>
                                    <div class="c"></div>
                                </div>
                            </div>
                            <!-- Thumbnail Item Skin End -->
                        </div>
                        <!-- Arrow Navigator -->
                        <span data-u="arrowleft" class="jssora05l" style="top:0px;left:248px;width:40px;height:40px;" data-autocenter="2"></span>
                        <span data-u="arrowright" class="jssora05r" style="top:0px;right:8px;width:40px;height:40px;" data-autocenter="2"></span>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6">
                    <ul class="list-unstyled">
                        <li class="price_nomargin"><p></p></li>
                        <li><p><label>Type:</label> {{$property->property_type}}</p></li>
                        <li><p><label>Bed Rooms:</label> {{$property->bed_rooms}}</p></li>
                        <li><p><label>Bath Rooms:</label> {{$property->bath_rooms}}</p></li>
                        <li><p><label>Reception:</label> {{$property->reception}}</p></li>
                        <li><p><label>Address:</label> {{$property->address}}</p></li>
                        {{--<li><p><label>Town:</label> {{$property->town}}</p></li>--}}
                    </ul>
                </div>
                <div class="col-xs-6 col-lg-6">
                    <div class="infor">
                        <p><label>Information:</label> {{$property->notes}}</p>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bg_white" >
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('status'))
                    <div id="success" class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <h2>Need More Information?</h2>
                {!! Form::open( array( 'url'=>'registerInterst' , 'files' => true ) ) !!}
                    {!! Form::hidden( 'property_reference', $property->reference, array( 'class' => 'form-control' ) ) !!}
                    {!! Form::hidden( 'property_address', $property->address, array( 'class' => 'form-control' ) ) !!}

                <div class="form-group">
                    <label>Email</label>
                    {!! Form::text( 'email', null, array( 'class' => 'form-control','placeholder' => 'Email' ) ) !!}
                </div>

                <div class="form-group">
                    <label>Phone</label>
                    {!! Form::text( 'phone', null, array( 'class' => 'form-control','placeholder' => 'Phone' ) ) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit( 'submit', array( 'class'=>'btn',  'value'=>'Send' ) ) !!}
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection