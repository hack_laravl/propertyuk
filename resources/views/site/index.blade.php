@extends('site.master')
@section('content')

    <div class="container">
    <div class="filter">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel filtertop">
                    <div class="panel-heading filetrtop_heading">
                        <div class="row">
                            <div class="col-lg-12 col-xs-offset-2">
                                <div class="col-xs-3   heding"><a class="active" href="#" id="forrent">Rent</a></div>
                                <div class="col-xs-3 heding"> <a>|</a> </div>
                                <div class="col-xs-3 heding"><a href="#" id="forsale">Sale</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        {!! Form::open( array( 'url'=>'search', 'class'=>'', 'style'=>'display: none;',   'id'=>'saleForm' ,'method' =>'GET' ) ) !!}
                        @include('site.searchform')
                        <div class="row">
                            <div class="col-md-12">
                                <label>Select price range:</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">£</div>
                                            <input type="text" name="salefrom" id="salefrom" class="form-control"readonly>
                                            <div class="input-group-addon">.00</div>
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon">£</div>
                                            <input type="text" name="saleto" id="saleto" class="form-control"readonly>
                                            <div class="input-group-addon">.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="slider-range-sale" class="pricetab"></div>
                                <div class="form-group">
                                    {!! Form::submit( 'Search', array( 'class'=>'btn btn-login searchbtn', 'id'=>'login-submit', 'value'=>'Log In', 'tabindex'=>'1' ) ) !!}
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}

                        {!! Form::open( array( 'url'=>'search', 'class'=>'',  'id'=>'rentFrom' ,'method' =>'GET' ) ) !!}
                        @include('site.searchform')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">£</div>
                                            <input type="text" name="from" id="from" class="form-control"readonly>
                                            <div class="input-group-addon">.00</div>
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon">£</div>
                                            <input type="text" name="to" id="to" class="form-control"readonly>
                                            <div class="input-group-addon">.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="slider-range-rent" class="pricetab"></div>
                                <div class="form-group">
                                    {!! Form::submit( 'Search', array( 'class'=>'btn btn-login searchbtn', 'id'=>'login-submit', 'value'=>'Log In', 'tabindex'=>'1' ) ) !!}
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

    <div style="min-height: 50px;">
        <!-- Jssor Slider Begin -->
        <!-- ================================================== -->
        <div id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto;
        top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
            <!-- Loading Screen -->
            <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
                top: 0px; left: 0px; width: 100%; height: 100%;">
                </div>
                <div style="position: absolute; display: block; background:{{ asset("img/loading.gif") }} no-repeat center center;
                        top: 0px; left: 0px; width: 100%; height: 100%;">
                </div>
            </div>
            <!-- Slides Container -->
            <div u="slides" style="position: absolute; left: 0px; top: 0px; width: 1300px; height: 400px; overflow: hidden;">
                <div>
                    <img u="image" src2="{{ asset("img/red.jpg") }}" />
                </div>
                <div>
                    <img u="image" src2="{{ asset("img/purple.jpg") }}" />
                </div>
                <div>
                    <img u="image" src2="{{ asset("img/blue.jpg") }}" />
                </div>
            </div>
        </div>
    </div>

@endsection