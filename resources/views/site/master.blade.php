<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>propertypointuk</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="propertypointuk">
    <meta name="razmy" content="propertypointuk">


    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("css/style.css") }}" rel="stylesheet">
    <link href="{{ asset("css/pricetab.css") }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.7.2/css/bootstrap-slider.css" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- The fav icon -->
    <link rel="shortcut icon" href="">
</head>
<body>
@include('site.header')
@yield('content')
@include('site.footer')

<script src="{{ asset("js/jquery-3.1.1.min.js") }}"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
<script src="{{ asset("js/bootstrap.min.js") }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.7.2/bootstrap-slider.js"></script>
<script src="{{ asset("js/jquery.ui.touch-punch.js") }}"></script>
<script src="{{ asset("js/custom.js") }}"></script>
<script src="{{ asset("js/jssor.slider-23.1.0.min.js") }}"></script>
<script src="{{ asset("js/docs.min.js") }}"></script>
<script src="{{ asset("js/pricetab.js") }}"></script>
<script src="{{ asset("js/property_slide.js") }}"></script>
<script src="{{ asset("BootstrapForm/dist/js/bootstrap-formhelpers.js") }}"></script>
<script src="{{ asset("BootstrapForm/js/bootstrap-formhelpers-states.js") }}"></script>
<script src="{{ asset("BootstrapForm/js/bootstrap-formhelpers-countries.js") }}"></script>
</body>
</body>
</html>
