<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Property extends Model
{
    const PUBLISHED = 1;
    const UN_PUBLISHED = 0;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function landlord(){
        return $this->belongsTo('App\Landlord' , 'landlord_id', 'id');
    }

    public function images(){
        return $this->hasMany('App\Images' ,'property_id','id');
    }


    public function moveImageTofolder($image){
        $fileName = $image->getFilename();
        $fileExtension = $image->getClientOriginalExtension();
        $newFile = $fileName.'.'.$fileExtension;
        $image->move(base_path() . '/public/uploads/', $newFile);
        return $newFile;
    }

    public function createPopertyByLandlord($data, $images){
        try{
            $property = Property::create($data);
            foreach ($images as $image){
                if(!empty($image)){
                    $newFile = $this->moveImageTofolder($image);
                    $imageData =[
                        'property_id' => $property->id,
                        'file' => $newFile
                    ];
                    $imageService = new Images();
                    $imageService->createImageForPropertId($imageData);
                }
            }


        }
        catch (\Exception $e){
            Log::error('can not create property'.$e->getMessage());
            abort(404);
        }
    }

    public function getAllPropertyWithLandloard(){
        try{
            $propeties = Property::with('landlord')->with('images')->paginate(20);
            return $propeties;
        }
        catch(\Exception $e){
            Log::error('can not load property'.$e->getMessage());
            abort(404);

        }
    }


    public function publishStatusChange($id){
        try{
            $property = Property::find($id);
            if($property->publish == self::PUBLISHED){
                $property->publish = self::UN_PUBLISHED;
            }
            elseif($property->publish == self::UN_PUBLISHED){
                $property->publish = self::PUBLISHED;
            }
            $property->save();
        }
        catch(\Exception $e){
            Log::error('Can not update publish status'.$e->getMessage());
            abort(404);

        }
    }

    public function findlandlordBypropertyId($id){
        try{
            $landlordPropoerty = Property::with('landlord')->with('images')->find($id);
            return $landlordPropoerty;
        }
        catch (\Exception $e){
            Log::error('can not find property'.$e->getMessage());
            abort(404);
        }

    }


    public function updatePropertyById($propertyId, $data, $images){
        try{
            $property = Property::find($propertyId);
            $property->update($data);
            foreach($images as $image){
                if(!empty($image)){
                    $newFile = $this->moveImageTofolder($image);
                    $imageData =[
                        'property_id' => $property->id,
                        'file' => $newFile
                    ];
                    $imageService = new Images();
                    $imageService->createImageForPropertId($imageData);
                }
            }

        }
        catch (\Exception $e){
            Log::error('can not update property'.$e->getMessage());
            abort(404);
        }

    }

    public function siteShowAllProperties(){
        try{
            $properties = Property::where('publish', 1)->paginate(20);
            return $properties;
        }
        catch (\Exception $e){
            Log::error('unable to load properties');
            abort(404);
        }
    }

    public static function findPropertyById( $id ){
        try{
            $property = Property::find( $id );
            return $property;
        }
        catch (\Exception $e){
            Log::error( 'Unable to find property ');
            abort(404);
        }
    }

    public static function searchFilter( Request $request, Property $property ){

        $property = $property->newQuery();
        $property->where('publish' ,1);

        if( $request->has('property_type') ){
            $property->where( 'property_type',$request->input('property_type') );
        }
        if( $request->has('search') ){
            $property->where( 'address','LIKE', "%{$request->input('search')}%" );
        }
        if( $request->has('bed_rooms') ){
            if( $request->input('bed_rooms') > 0 ){
                $property->where( 'bed_rooms',$request->input('bed_rooms') );
            }
        }
        if( $request->has('bath_rooms') ){
            if( $request->input('bath_rooms') > 0 ){
                $property->where( 'bath_rooms',$request->input('bath_rooms') );
            }
        }
        if( $request->has('reception') ){
            if( $request->input('reception') > 0 ){
                $property->where( 'reception',$request->input('reception') );
            }
        }

        return $property->get();

    }

    public static function deleteById( $id ){
        try{
            $img_id_array = [];
            $image = new Images();

            $property = Property::with('images')->find( $id );
            foreach ($property->images as $image){
                $img_id_array[] = $image->id;
            }
            $image->deleteImageByIds( $img_id_array );

            $property->delete();
        }
        catch (\Exception $e ){
            Log::error('Unable to delete property Id: '.$id . ' '. $e->getMessage());
            abort(404);
        }

    }

}
