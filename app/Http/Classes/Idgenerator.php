<?php
/**
 * Created by PhpStorm.
 * User: test
 * Date: 05/03/2017
 * Time: 21:35
 */

namespace App\Http\Classes;


class Idgenerator
{

    public function createIdForLandlord(){
        $prefix = 'PPUKLAN';
        $numbers = rand(10000000, 99999999);
        $id = $prefix.$numbers;
        return $id;
    }


    public function createIdForProperty(){
        $prefix = 'PPUKPRO';
        $numbers = rand(10000000, 99999999);
        $id = $prefix.$numbers;
        return $id;
    }
}