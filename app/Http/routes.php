<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SiteController@siteHomePage');
Route::get('adminLogin', 'AdminController@showLogin');
Route::get('propertyById/{id}', 'SiteController@ShowPropertyById');
Route::post('login', 'AdminController@doLogin');
Route::post('registerInterst', 'SiteController@CreatePropertyInterst');
Route::get('logout', 'AdminController@getSignOut');
Route::get('search', 'SiteController@SearchProperty');





Route::group(['middleware' => ['auth']], function() {
    Route::get('admin', 'AdminController@dashboard' );
    Route::get('showPropertyForm', 'AdminController@showPropertyForm' );
    Route::post('createProperty', 'AdminController@createProperty' );
    Route::get('togglePublishStatus/{id}', 'AdminController@publishUnpublished' );
    Route::get('listoflandlords', 'AdminController@showLandlordList' );
    Route::get('showTenantFrom', 'AdminController@showTenantForm' );
    Route::get('editPropertyFrom/{id}', 'AdminController@editProperty' );
    Route::get('deleteImage', 'AdminEditController@deleteImages' );
    Route::post('updateProperty', 'AdminEditController@updateProperty' );
    Route::get('showCreateForm', 'AdminController@showCreateAccount');
    Route::post('createUser', 'AdminController@createAccount' );
    Route::get('showIntrests', 'AdminController@ShowAllIntersts' );

    //landlord CRUD
    Route::get( 'editLandlord/{id}' , 'AdminLandlordController@editLandloardById' );
    Route::post( 'updateLandlord' , 'AdminLandlordController@updateLandlordById' );
    Route::get( 'deleteLandlord/{id}' , 'AdminLandlordController@deleteLandlordById' );
    Route::get( 'showaddPropertyform/{id}' , 'AdminLandlordController@showPropertyToLandlordForm' );

    //Property CRUD
    Route::get('deleteProperty/{id}','AdminPropertyController@deletePropertyById');
});

