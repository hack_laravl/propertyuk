<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminPropertyController extends Controller
{
    public function deletePropertyById ( $id ){
        $property = new Property();
        $propertyData = $property->findlandlordBypropertyId( $id );
        Property::deleteById( $id );
        return redirect('editLandlord/'.$propertyData->landlord->id )->with( 'status', 'Property Deleted' );
    }
}
