<?php

namespace App\Http\Controllers;

use App\Images;
use App\Property;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class AdminEditController extends Controller
{
    public function deleteImages(Request $request){
        $propertyId = $request->input('id');
        $images = $request->input('image');
        $imageService = new Images();
        $imageService->deleteImageByIds($images);
        return Redirect::to('editPropertyFrom/'.$propertyId);
    }
    
    public function updateProperty(Request $request){
        $this->validate($request,[
            'address' => 'required',
            'postcode' => 'required|min:6|max:6',
            'price' => 'required|numeric',
            'bed_rooms' => 'required|numeric',
            'bath_rooms' => 'required|numeric',
            'reception' => 'required|numeric',
            'property_type' => 'required',
            'category' => 'required',
        ]);
        $data = [
            'address' => $request->input('address'),
            'postcode' => $request->input('postcode'),
            'price' => $request->input('price'),
            'price_tag' => $request->input('price_tag'),
            'bed_rooms' => $request->input('bed_rooms'),
            'bath_rooms' => $request->input('bath_rooms'),
            'reception' => $request->input('reception'),
            'property_type' => $request->input('property_type'),
            'category' => $request->input('category'),
            'publish' => $request->input('publish'),
            'notes' => $request->input('notes'),
        ];
        $propertyId = $request->input('id');

        $images = $request->file('image');
        $propertyServices = new Property();
        $propertyServices->updatePropertyById($propertyId, $data ,$images);
        return Redirect::to('editPropertyFrom/'.$propertyId);
    }
}
