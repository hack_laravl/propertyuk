<?php

namespace App\Http\Controllers;

use App\Images;
use App\Property;
use App\PropertyInrest;
use Illuminate\Http\Request;

use App\Http\Requests;

class SiteController extends Controller
{
    public function siteHomePage(){
        return view('site.index');
    }

//    public function searchRentProperties(){
//        $property = new Property();
//        $properties = $property->siteShowAllProperties();
//
//        return view('site.searchRent')->with('properties', $properties);
//    }
    
    public function getImageForSite( $property_id ){
        $findImages = new Images();
        $images = $findImages->getImagesByPropertyId( $property_id );
        if( count( $images ) > 0){
            return $images[0]['file'];
        }
        else{
            return 'default.png';
        }

    }

    public function getAllImageForProperty( $property_id ){
        $images = [];
        $findImages = new Images();
        $images = $findImages->getImagesByPropertyId( $property_id );
        return $images;

    }
    
    public function ShowPropertyById( $id ){
        $property = Property::findPropertyById( $id );

        return view('site.property_page')->with('property', $property);
    }
    
    public function SearchProperty(Request $request, Property $property){
        $properties = Property::searchFilter( $request, $property );

        return view('site.searchRent')->with('properties', $properties);
    }

    public function CreatePropertyInterst( Request $request ){
        $this->validate($request,[
            'email' => 'email|required',
            'phone' => 'required',
        ]);
        $interts = new PropertyInrest();
        $interts->createInterest( $request->all() );
        return redirect()->back();


    }
}
