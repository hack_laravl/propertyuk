<?php

namespace App\Http\Controllers;

use App\Landlord;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminLandlordController extends Controller
{
    public function editLandloardById( $id ){
        $landlord = Landlord::getLandlordById( $id );
        return view('admin.editLandlord')->with( 'landlord', $landlord );
    }

    public function updateLandlordById( Request $request ){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'landlord_address' => 'required',
            'landlord_postcode' => 'required',
            'phone_number' => 'required',
            'landlord_notes' => 'required',
        ]);
        $data =[
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'landlord_address' => $request->input('landlord_address'),
            'landlord_postcode' => $request->input('landlord_postcode'),
            'phone_number' => $request->input('phone_number'),
            'landlord_notes' => $request->input('landlord_notes'),
        ];
        Landlord::updateLandlordById( $request->input('id'), $data );
        return redirect('editLandlord/'.$request->input('id') )->with('status', 'Landlord Updated');
    }

    public function deleteLandlordById( $id ){
        $landlord = Landlord::deleteLandlordById( $id );

        if( $landlord ){
            return redirect( 'listoflandlords' )->with( 'status', 'Landlord Deleted' );
        }
        else{
            return redirect( 'listoflandlords' )->withErrors( 'System found properties belongs to this landlord, Please delete them and try again' );
        }
    }

    public function showPropertyToLandlordForm ( $id ){
        $landlord = Landlord::getLandlordById( $id );
        return view(  'admin.add_property' )->with( 'landlord', $landlord );
    }

}
