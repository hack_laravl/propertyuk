<?php

namespace App\Http\Controllers;

use App\Http\Classes\Idgenerator;
use App\Landlord;
use App\Property;
use App\PropertyInrest;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
    public function dashboard(){
        $property = new Property();
        $listOfProperties = $property->getAllPropertyWithLandloard();
        return view('admin.dashboard')->with('listOfProperties',$listOfProperties);
    }

    public function showPropertyForm(){
        return view('admin.create_property');
    }

    public function createProperty(Request $request){
        $landlordData= [];
        $idgenerator = new Idgenerator();
        $ladnlordId = $idgenerator->createIdForLandlord();
        $propertyId = $idgenerator->createIdForProperty();
        $landlord_id = $request->input('landlord_id');
        if ( !$landlord_id ){
            $this->validate($request,[
                'name' => 'required',
                'email' => 'required|unique:landlords,email',
                'landlord_address' => 'required',
                'landlord_postcode' => 'required',
                'phone_number' => 'required|numeric',
            ]);
        }

        $this->validate($request,[
            'address' => 'required',
            'postcode' => 'required|min:4|max:10',
            'price' => 'required|numeric',
            'bed_rooms' => 'required|numeric',
            'bath_rooms' => 'required|numeric',
            'reception' => 'required|numeric',
            'property_type' => 'required',
            'category' => 'required',
        ]);

        
        $landlordData =[
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'landlord_address' => $request->input('landlord_address'),
            'landlord_postcode' => $request->input('landlord_postcode'),
            'phone_number' => $request->input('phone_number'),
            'landlord_notes' => $request->input('landlord_notes'),
            'reference' => $ladnlordId,
        ];
        $propertyData = [
            'address' => $request->input('address'),
            'postcode' => $request->input('postcode'),
            'town' => $request->input('town'),
            'bed_rooms' => $request->input('bed_rooms'),
            'bath_rooms' => $request->input('bath_rooms'),
            'reception' => $request->input('reception'),
            'property_type' => $request->input('property_type'),
            'category' => $request->input('category'),
            'notes' => $request->input('notes'),
            'price' => $request->input('price'),
            'price_tag' => $request->input('price_tag'),
            'reference' => $propertyId,
        ];

        $images = $request->file('image');

        $landlordServices = new Landlord();
        $landlordServices->createProperty( $landlordData, $propertyData, $images ,$landlord_id );
        if( $landlord_id ){
            return redirect( 'editLandlord/'.$landlord_id )->with('status', 'Property added');
        }
        else{
            return redirect('showPropertyForm')->with('status', 'Property added');
        }

    }
    
    public function publishUnpublished($id){
        $property = new Property();
        $property->publishStatusChange($id);
        return back();
    }
    
    public function showLandlordList(){
        $landlordService = new Landlord();
        $lisoflandlords = $landlordService->listofLandlords();

        return view('admin.landlords')->with( 'lisoflandlords', $lisoflandlords );
    }


    public function showTenantForm(){
        return view('admin.tenantForm');
    }
    
    
    public function editProperty($id){
        $propertyService = new Property();
        $property = $propertyService->findlandlordBypropertyId($id);
        return view('admin.editProperty')->with( 'property', $property );
    }

    public function showCreateAccount(){
        return view('admin.userCreate');
    }

    public function createAccount(Request $request){
        $this->validate($request,[
            'username' => 'required',
            'email' => 'email|required|unique:users,email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8'
        ]);
        User::createUser( $request->all() );
        return redirect('showCreateForm')->with('status','User created successfully');
    }

    public function showLogin(){
        if(Auth::check()){
            return redirect('admin');
        }
        else{
            return view('admin.login');
        }
    }

    public function doLogin( Request $request ){
        $this->validate( $request, [
            'email' => 'email|required',
            'password' => 'required',
        ]);
        $data =[
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        if ( Auth::attempt( $data )){
            return redirect('admin');
        }
        else{
            return redirect('adminLogin');
        }
    }

    public function getSignOut(Request $request) {

        Auth::logout();
        return redirect('adminLogin');

    }

    public function ShowAllIntersts(){
        $propertyIntrests = new PropertyInrest();
        $data = $propertyIntrests->showAllPropertiesIntrest();

        return view('admin.intersts')->with( 'propertIntersts', $data );

    }
}
