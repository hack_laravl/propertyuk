<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class Images extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function createImageForPropertId($data){
        try{
            Images::create($data);
        }
        catch (\Exception $e){
            Log::error('Can not create image'.$e->getMessage());
            abort(404);
        }
    }

    public function deleteImagePath($image){
        $filePath = base_path() . '/public/uploads/'.$image;
        File::delete($filePath);
    }
    
    public function deleteImageByIds( $data ){
        try{
            foreach ($data as $id){
                $image = Images::find($id);
                $this->deleteImagePath($image->file);
                $image->delete();
            }
        }
        catch(\Exception $e){
            Log::error('Can not delete image'.$e->getMessage());
            abort(404);
        }
    }

    public function getImagesByPropertyId( $property_id ){
        try{
            $images = Images::where( 'property_id', $property_id )->get();
            return $images;
        }
        catch (\Exception $e){
            Log::error('unable to find images'. $e->getMessage());
            abort(404);
        }

    }
    
    
}
