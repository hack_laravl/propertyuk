<?php

namespace App;

use App\Http\Classes\Constance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Landlord extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'landlords';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function properties(){
        return $this->hasMany('App\Property', 'landlord_id', 'id');
    }

    


    public function createProperty($landlordData, $propertyData, $images, $landloard_id ){
        try{
            if ( !$landloard_id ){
                $landlordData['status'] = Constance::ACTIVE;
                $landlord = Landlord::create($landlordData);
                $propertyData['landlord_id'] = $landlord->id;
            }
            else {
                $propertyData['landlord_id'] = $landloard_id;
            }
            $property = new Property();
            $property->createPopertyByLandlord($propertyData, $images);
        }
        catch (\Exception $e){
            Log::error('Can not create landlord'.$e->getMessage());
            abort(404);

        }
    }

    public function listofLandlords(){
        try{
            $listofLandlords = Landlord::with('properties')->where('status', 1)->paginate(20);
            return $listofLandlords;

        }
        catch(\Exception $e){
            Log::error('can not load landlords'.$e->getMessage());
            abort(404);
        }
    }

    static public function getLandlordById( $id ){
        try{
            $landlord = Landlord::with('properties')->find( $id );
            return $landlord;
        }
        catch (\Exception $e){
            Log::error('can not get landlord By ID:'.$id.'  '.$e->getMessage());
            abort(404);
        }
    }

    static public function updateLandlordById ( $id , $data ){
        try{
            $landlord = Landlord::find( $id );
            $landlord->update( $data );
        }
        catch (\Exception $e ){
            Log::error ('Unable to update landlord ID: ' .$id. '' . $e->getMessage());
            abort(404);
        }
    }

    static public function deleteLandlordById( $id ){
        try {
            $lanlord = Landlord::with('properties')->find( $id );
            $count = count( $lanlord->properties );
            if($count > 0){
                return false;
            }
            else{
                $lanlord->delete();
            }
        }
        catch (\Exception $e){
            Log::error ('Unable to Delete landlord ID: ' .$id. '' . $e->getMessage());
            abort(404);
        }
    }

    static public function findLandlordIdbyProperty( $id ){
        try {
            $property = Property::findPropertyById( $id );
            return $property->landlord_id;
        }
        catch (\Exception $e){

        }
    }
    

}
