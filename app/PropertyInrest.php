<?php

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class PropertyInrest extends Model
{
    protected $table = 'property_inrests';

    protected $guarded = [];

    public function createInterest( $data ){
        try{
            $property = PropertyInrest::create($data);
        }
        catch (\Exception $e) {
            Log::error('can not create propertyInterst' . $e->getMessage());
            abort(404);
        }

    }

    public function showAllPropertiesIntrest(){
        try{
            $propertyIntersts = PropertyInrest::all();
            return $propertyIntersts;
        }
        catch (\Exception $e){
            Log::error('unable to load property Intersts'. $e->getMessage());
            abort(404);
        }
    }

}
